#!/bin/bash

uname="$(cat $POSTGRES_USER_FILE)"
pass="$(cat $POSTGRES_PASSWORD_FILE)"

ga_ip="$(cat $GA_IP)"
ga_uname="$(cat $GA_USER_FILE)"
ga_pass="$(cat $GA_PASSWORD_FILE)"
ga_db="$(cat $GA_DB_FILE)"
ga_meta_db="$(cat $GA_DB_META_FILE)"

sed -i "s/pg_uname/${uname}/" /opt/dagster/dagster_home/dagster.yaml
sed -i "s/pg_pass/${pass}/" /opt/dagster/dagster_home/dagster.yaml

sed -i "s/games_analytics_ip_ph/${ga_ip}/" ~/.bashrc
sed -i "s/games_analytics_uname_ph/${ga_uname}/" ~/.bashrc
sed -i "s/games_analytics_pass_ph/${ga_pass}/" ~/.bashrc
sed -i "s/games_analytics_datamart_dbname_ph/${ga_db}/" ~/.bashrc
sed -i "s/games_analytics_meta_dbname_ph/${ga_meta_db}/" ~/.bashrc

source ~/.bashrc

touch /etc/crontab /etc/cron.*/*

service cron start

dagster-daemon run &

dagit -h 0.0.0.0 -p 3001
