# aig-analytics-dagit

Dagit deployment repo

# Setup

1. Open _docker_compose.yml_ file.
1. Create files listed under _secrets_ section so that they only contain a line with a secret.
1. Under _dagster-dagit_ replace tag placeholder in _image_.
1. Close _docker_compose.yml_ file.
1. Run `docker-compose up -d`
