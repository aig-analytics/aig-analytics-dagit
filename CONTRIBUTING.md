# Contributing

## Branching

Create an issue ticket in Gitlab board. Then checkout a branch in a `<NUMBER>-what-is-branch-about` format where `<NUMBER>` is the ID of the ticket. This way the issue will be automatically mentioned when opening Merge Request and closed after the merge.

## Commit messages

This repository utilizes [semantic-release](https://github.com/semantic-release/semantic-release). It expects commits to follow the [conventional commits style](https://www.conventionalcommits.org/en/v1.0.0/). For easier control over commit message style we utilize [commitizen](https://github.com/commitizen-tools/commitizen).

1. Install commitizen and pre-commit

    `pip install commitizen pre-commit`
1. Install git hooks

    `pre-commit install-hooks`
1. Install commit message type hooks

    `pre-commit install --hook-type commit-msg`

1. Commit changes with _commitizen_

    `cz commit`
