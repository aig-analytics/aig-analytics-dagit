FROM python:3.7.9-slim-buster

EXPOSE 3000
ENTRYPOINT ["/opt/dagster/entrypoint.sh"]
WORKDIR /opt/dagster

RUN apt update -y && \
    apt install -y cron && \
    rm -rf /var/lib/apt/lists/*

COPY . .

RUN pip install -r dag_reqs.txt &&  \
    mkdir dagster_home

RUN ln -s dagit_workspace/workspace.yaml workspace.yaml && \
  mkdir -p apps/aig-analytics-scraper && \
  chmod +x entrypoint.sh && \
  echo 'export DAGSTER_HOME=/opt/dagster/dagster_home' >> ~/.bashrc && \
  echo 'export GAMES_ANALYTICS_IP="games_analytics_ip_ph"' >> ~/.bashrc && \
  echo 'export GAMES_ANALYTICS_PORT=5432' >> ~/.bashrc && \
  echo 'export GAMES_ANALYTICS_USER="games_analytics_uname_ph"' >> ~/.bashrc && \
  echo 'export GAMES_ANALYTICS_USER_PASSWORD="games_analytics_pass_ph"' >> ~/.bashrc && \
  echo 'export GAMES_ANALYTICS_META_DATABASE="games_analytics_meta_dbname_ph"' >> ~/.bashrc && \
  echo 'export GAMES_ANALYTICS_DATAMART_DATABASE="games_analytics_datamart_dbname_ph"' >> ~/.bashrc
# COPY example_repo.py /opt/dagster/


